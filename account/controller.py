from account import model
from flask import Blueprint,session
from flask import render_template,request
from flask import redirect,url_for
from flask import session
from flask import jsonify
import bson
from werkzeug.security import generate_password_hash, check_password_hash
from werkzeug.utils import secure_filename
from datetime import datetime
import uuid
from bson.objectid import ObjectId	
import os

permit_names = set(['png', 'jpg', 'jpeg', 'gif'])

blll = Blueprint('guygiuschoisd', __name__)
def image_upload():
	def permissible(fname):
		return '.' in fname and fname.split(".",1)[1] in permit_names
	files = request.files['profile_pic']
	fname = files.filename
	isitallow = permissible(fname)
	print isitallow
	if isitallow:
		filename = secure_filename(fname)
		files.save(os.path.join('static/css/img', filename))
		params = {key: value for key,value in request.form.iteritems()}
		params['profile_pic'] = os.path.join('static/css/img', filename)
		return params['profile_pic']
	return None

@blll.route('/create',methods=['GET','POST'])
def create():
	print "00"
	if request.method == 'POST':
		params = {key: value for key,value in request.form.iteritems()}
		password=generate_password_hash(request.form['password'],method='pbkdf2:sha512',salt_length=25)
		params['password'] = password
		params['created_at'] = datetime.now()
		params['profile_pic'] = image_upload()
		params['verified'] = False
		params['auth_token'] = None
		result=model.insert(**params)
		return redirect(url_for('guygiuschoisd.login'))
   	return render_template("createaccount.html")
@blll.route('/upload', methods=['POST'])
def upload():
	def permissible(fname):
		return '.' in fname and fname.split(".",1)[1] in permit_names
	files = request.files['profile_pic']
	
	fname = files.filename
	
	isitallow = permissible(fname)
	print isitallow
	if isitallow:
		filename = secure_filename(fname)
		files.save(os.path.join('/home/vishvajit/flask_project/demos/mvc_demo/files', filename))
		params = {key: value for key,value in request.form.iteritems()}
		params['profile_pic'] = os.path.join('/home/vishvajit/flask_project/demos/mvc_demo/files', filename)
		
		return jsonify({'ok':True,"Msg":"image successfully uploaded"})
	return jsonify({'ok':False,"Msg":"image formate not supported"})
	
@blll.route('/login',methods=['GET','POST'])
def login():

	if request.method == 'POST':
		data=request.json['name']
		para = {"user_name": request.json['name']}
		if 'user_name' in session:
			return jsonify(**{'ok':True	,'data':data})
		 	'''return redirect(url_for("guygiuschoisd.successfully"))'''	
		search=list(model.find(**para))
		if search != []:
		 	s = search[0]
		 	password = request.json['password']
		 	dpassword = s['password']

		 	if check_password_hash(dpassword, password):
			
		 		session['user_name'] = request.json['name']
		 		session['password'] = s['password']

		 		return jsonify(**{'ok':True	, 'data': data})
		 		'''return redirect(url_for("guygiuschoisd.successfully",name=name))'''
		 	return jsonify(**{'ok':False,'message':"Enter Correct Info"})
		return jsonify(**{'ok':False,'message':" no such user_name"})
	return render_template("Homepage.html")
@blll.route('/logout')
def logout():
	if 'user_name' in session:

		session.pop('user_name')
		'''return jsonify(**{"ok":True,'msg':"loged_out successfully"})'''
		return redirect(url_for("guygiuschoisd.login"))
	return jsonify(**{"ok":False,'msg':"already_loged_out"})

@blll.route('/delete', methods=['DELETE'])
def delete():
	para = {'user_name':session.pop('user_name'),'password':session.pop('password')}
	model.delete(**para)
	return jsonify(**{'ok':True	,'msg':"Delete_successfully"})
	
@blll.route('/update',methods=['PUT'])
def update():
	name = session['user_name']
	password = session['password']
	print name
	print password
	#rname = request.form['uname']
	#rpsw = request.form['psw']
	#params={key:value for key,value in request.form.iteritems() if password == rpsw and name == rname}
	
	params={key:value for key,value in request.form.iteritems()}

	print params
	params['profile_pic'] = image_upload()
	params['verified'] = False
	params['auth_token'] = None
	result = model.update(name,password,**params)
	return jsonify(**{'ok': result})
	
@blll.route('/successfully',methods=['GET','POST'])
def successfully():
	para = {"user_name": session['user_name']}
	user_name=session['user_name']
	img_add=list(model.find(**para))
	img_address = img_add[0]
	add_of_pic = img_address['profile_pic']
	posts = model.find1(**para)
	dictn=[]
	for each in reversed(posts):
		each['_id'] = str(each['_id'])
		each['inserted_at'] = datetime.strftime(each['inserted_at']," %a , %d-%b-%Y , %H:%M")
		dictn.append(each)
	return render_template("successfully.html",add_of_pic=add_of_pic,user_name=user_name,all_posts=dictn)

@blll.route("/post_blog" , methods=['POST'])
def post_blog():
	if request.method == 'POST':
		user_name=session["user_name"]
		dictn=[]
		post_blog=request.json
		post_blog['user_name'] = session["user_name"]
		model.insert1(**post_blog)
		blog= model.find1(**{'user_name': user_name})
		for items in reversed(blog):
			items["_id"] = str(items['_id'])
			items['inserted_at'] = datetime.strftime(items['inserted_at']," %a , %d-%b-%Y , %H:%M")
			dictn.append(items)
		return jsonify(**{"data":dictn})

@blll.route('/delete_post/<d>',methods = ['DELETE'])
def delete_post(d):
	user_name = session["user_name"]
	post_list=[]
	model.delete1(d)
	blog = model.find1(**{"user_name" : user_name})
	for items in reversed(blog):
		items["_id"] = str(items['_id'])
		post_list.append(items)
	return jsonify(**{"data":post_list})


@blll.route('/search_pepele')
def search_pepele():
	user=request.args.get('user')
	para = {"user_name": user}
	user = para['user_name']
	if user:
		return jsonify(**{"ok":True})
	else:
		return jsonify(**{"ok" : False})

@blll.route('/pepele_found')
def pepele_found():
	user=request.args.get('user')
	request_from = session['user_name']
	para = {"user_name": user}
	user_name=user
	img_add=list(model.find(**para))
	img_address = img_add[0]
	add_of_pic = img_address['profile_pic']
	posts = model.find1(**para)
	dictn=[]
	for each in reversed(posts):
		each['_id'] = str(each['_id'])
		each['inserted_at'] = datetime.strftime(each['inserted_at']," %a , %d-%b-%Y , %H:%M")
		dictn.append(each)
	return render_template("search_friends.html",add_of_pic=add_of_pic,user_name=user_name,all_posts=dictn,request_from=request_from)

@blll.route('/forgot_password',methods=['GET','POST'])
def forgot_password():
	if request.method == 'POST':
		user = request.json['email']
		searched =list(model.find1(**{'user_name':user}))
		searched[0]
		for each in searched():
			each['auth_token'] = uuid.uuid4()
		return auth
	return render_template('reset_password.html')

@blll.route('/like_dislike',methods = ['PUT'])
def like_dislike():
	likes_list=[]
	likes = request.json
	n_likes = likes["likes"]
	id_likes = likes["_id"]
	model.update1(id_likes,**{"likes" : n_likes})
	# posts_likes = model.find1(**{'_id':ObjectId(id_likes)})
	posts_likes = map_find('_id', ObjectId(id_likes))
	print "smcsab,fbasfkba"
	print posts_likes
	# maplist=map(lambda each : str(each['_id']), posts_likes)
	# print "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx"
	# print maplist
	for each in posts_likes:
		each['_id'] = str(each['_id'])
		likes_list.append(each)
		print "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx"

		print likes_list
	return jsonify(**{"success_callback":likes_list})


def map_find(key,value):
	find_list = model.find1(**{key : value})
	return find_list

@blll.route("/about_us")
def about_us():
	return render_template('about_us.html')
